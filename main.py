class Dog:
    species = 'mammal'

    def __init__(self, name, age, color):
        self.name = name
        self.age = age
        self.color = color

    def __repr__(self):
        return f'{self.name, self.age, self.color}'


jake = Dog("Jake", 7, 'black')
doug = Dog("Doug", 4, 'red')
william = Dog("William", 5, 'white')


def get_biggest_number(*args):
    return max(args)


def get_smallest_number(*args):
    return min(args)


print(f"The oldest dog is {get_biggest_number(jake.age, doug.age, william.age)} years old.")
print(f"The smallest dog is {get_smallest_number(jake.age, doug.age, william.age)} years old.")
print(jake)
print(doug)
print(william)


